#include "App.h"
#include <Agmd3D/Core/MediaManager.h>
#include <Agmd3D/Core/Driver.h>
#include <Agmd3D/Core/Enums.h>
#include <Agmd3D/Core/Declaration.h>
#include <Agmd3D/Core/DeclarationElement.h>
#include <Agmd3D/Core/ResourceManager.h>
#include <Agmd3D/Core/Model/GeometryFactory.h>

#include <Agmd3D/Core/Buffer/FrameBuffer.h>

#include <Agmd3D/Core/RenderObject/GraphicString.h>
#include <Agmd3D/Core/Model/IcosahedronModel.h>
#include <Agmd3D/Core/Model/Scene.h>
#include <Agmd3D/Core/Model/Water.h>
#include <Agmd3D/Core/Model/SkyBox.h>
#include <Agmd3D/Core/Model/Light.h>
#include <Agmd3D/Core/Model/Material.h>

#include <Agmd3D/Core/SceneNode/MeshNode.h>
#include <Agmd3D/Core/SceneNode/CameraNode.h>

#include <Agmd3D/Core/RenderingMode/DeferredRendering.h>
#include <Agmd3D/Core/RenderingMode/ForwardRendering.h>

#include <Agmd3D/Core/Camera/FPCamera.h>
#include <Agmd3D/Core/Camera/FollowCamera.h>

#include <AgmdUtilities/Utilities/Color.h>

#include <Agmd3D/Core/Effects/PostEffectMgr.h>
#include <Agmd3D/Core/Effects/AntiAliasing.h>

#include <Agmd3D/Core/Model/Model.h>



#include <Agmd3D/Core/GUI/GUIMgr.h>
#include <Agmd3D/Core/GUI/ASlider.h>
#include <Agmd3D/Core/GUI/AWindow.h>

#include <Agmd3D/Core/Shader/ShaderPreCompiler.h>
#include <Agmd3D/Core/Controller/FirstPersonController.h>


#include <Agmd3D/Core/Tools/Statistics.h>
#include <Renderer/OpenGL/GlDriver.h>
#include <Agmd3D/Core/Tools/Fast2DSurface.h>
#include <Agmd3D/Core/Tools/ColorPicking.h>



#include <glm/ext.hpp>
#include <libnoise/noise.h>

#define _USE_MATH_DEFINES
#include <math.h>
#include <random>

#include <Core/Shader/ShaderPipeline.h>

using namespace Agmd;
using namespace AgmdUtilities;

SINGLETON_IMPL(App);
void App::Run(int argc, char** argv)
{
	if(argc > 0)
	{
		File main(argv[0]);
		MediaManager::Instance().AddSearchPath(main.Path());
		ShaderPreCompiler::Instance().AddSearchPath(main.Path()+"/Shader");
	}

	wxEntryStart( argc, argv );
	AgmdApplication::Run(argc,argv);
}

void App::MakeWindow()
{
	//wxApp::SetInstance(m_wxApplication);
}

#define LINE (16*10)
#define MAX_SPHERE (LINE*LINE)

void Bench(SceneNode* parent)
{

    MeshNode* node;
    Model* model = new Icosahedron(2);
    for(a_uint32 i = 0; i < MAX_SPHERE; i++)
    {
        
        node = new MeshNode(model);
        float x = (i%LINE)*2.0f;
        float y = (i/LINE)*2.0f;
        node->getTransform().translate(x,y,0);
        parent->addChild(node);
    }

}


SceneNode* createLight(LightType type,Model* lmodel, Material* lmodelMat,float range = 10.0f)
{

    MeshNode* _node = new MeshNode(lmodel);
    _node->getTransform().scale(0.1f,0.1f,0.1f);
    _node->setMaterial(lmodelMat);
    Light* light = new Light(vec3(0),vec3(0),type);
    light->SetRange(range);
    //light->SetAngles(40,80.0f);
    LightNode* l = new LightNode(NULL,light);
    _node->addChild(l);
    return _node;
}

void App::init()
{     

	m_MatProj3D = glm::perspective(35.0f, (float)getScreen().x / (float)getScreen().y, 0.01f, 1000.f);
	m_MatProj2D = ortho(0.0f,(float)getScreen().x,0.0f,(float)getScreen().y);
	ForwardRendering* mode = new ForwardRendering(getScreen());
	RenderingMode::setRenderingMode(mode);
	m_fps = new GraphicString(ivec2(0,0),"",Color::black,"Arial",20);
	m_Scene = new SceneMgr();
	Model* mesh;
	MeshNode* node;

    //lighting model shader
    ShaderPipeline* _default= ShaderPipeline::GetDefaultPipeline();
    ShaderPipeline * lightPipe = new ShaderPipeline(*_default);
    ShaderProgram diffuseShader;
    diffuseShader.LoadFromFile("Shader/light_shader.glsl");

    lightPipe->setShader(diffuseShader,RENDERPASS_DIFFUSE);
    Material* lMat = new Material(lightPipe);



	GUIMgr& guimgr = GUIMgr::Instance();

    Image img[6];
    const char* cubemap_str[] = {
        "right",
        "left",
        "down",
        "up",
        "back",
        "front"
    };
    for(int i = 0; i < 6; i++)
    {
        img[i] = Image(ivec2(512));
        img[i].LoadFromFile(StringBuilder("texture/skybox/")(cubemap_str[i])(".tga"));
    }
    Texture texCube;
    texCube.CreateFromImage(img,PXF_A8R8G8B8);
	SkyBox* sk = new SkyBox();
    sk->SetTexture(texCube);
	m_Scene->SetSkybox(sk);
	//Light* l = new Light(vec3(),vec3(0,1,1),LIGHT_DIR);
	//m_Scene->AddLight(l);
 	Driver::Get().SetActiveScene(m_Scene);
	Driver::Get().SetCullFace(2);
	//cam3D = new Camera(m_MatProj3D);
	cam3D = new Camera(PROJECTION_PERSPECTIVE,ProjectionOption(vec2(getScreen()),60.0f,0));//new Camera(PROJECTION_PERSPECTIVE,ProjectionOption(vec2((float)getScreen().x,(float)getScreen().y)));//new Camera(PROJECTION_ORTHO,ProjectionOption(vec4(-0.1f,0.1f,-0.1f,0.1f)));//
	InputController* controller = new FirstPersonController();
	camNode = new CameraNode(cam3D,controller);
	camNode->setController(controller);
    
    Model* ligthModel = MediaManager::Instance().LoadMediaFromFile<Model>("model/light.obj");
    SceneNode* _node;
    _node = createLight(LIGHT_POINT,ligthModel,lMat);
    m_Scene->AddNode(_node); 
    _node = createLight(LIGHT_SPOT,ligthModel,lMat,5.0f);
    m_Scene->AddNode(_node); 
    _node = createLight(LIGHT_SPOT,ligthModel,lMat,6.0f);
    m_Scene->AddNode(_node); 
    m_Scene->AddNode(camNode);


    Model* plane = GeometryFactory::createPlane(ivec2(10),ivec2(1));
    Material* planeMat = new Material();
    Texture diffuse,
            normal;
    diffuse.CreateFromFile("Texture/crate_d.jpg",PXF_A8R8G8B8);
    normal.CreateFromFile("Texture/crate_n.jpg",PXF_A8R8G8B8);
    planeMat->SetTexture(diffuse,0,(TRenderPass)(1 << RENDERPASS_DIFFUSE));
    planeMat->SetTexture(normal,1,(TRenderPass)(1 << RENDERPASS_DIFFUSE));
    MeshNode*_mesh = new MeshNode(plane);
    _mesh->setMaterial(planeMat);
    _mesh->getTransform().rotate(-90,vec3(1,0,0));
    m_Scene->AddNode(_mesh);


    


	//cam2D = new Camera(m_MatProj2D);
	cam2D = new Camera(PROJECTION_ORTHO,ProjectionOption(vec4(0,1000.0f,0,1000.0f)));

	m_debugWindow = new AWindow();
	Texture& t = ColorPicking::Instance().getPickingScreen();
	m_debugWindow->SetBackground(t);
	//GUIMgr::Instance().AddWidget(m_debugWindow);
	//Bench(m_Scene->GetRoot());
	Camera::setCurrent(cam3D, CAMERA_3D);
	Camera::setCurrent(cam2D, CAMERA_2D);
}

void App::OnUpdate(a_uint64 time_diff/*in ms*/)
{
}

void App::OnRender3D()
{
    
}

void App::OnRender2D()
{
    Statistics& stats = Driver::Get().GetStatistics();
    *m_fps = StringBuilder("Draw count : ")(stats.GetDrawCount())(", FPS : ")(stats.GetFps())
        (", drawTimer : ")(stats.GetMainTime())(" us"); 
    m_fps->draw();
}

void App::OnClick( int click, vec2 pos,bool up)
{

	
	AgmdApplication::OnClick(click,pos,up);

	SceneNode* n = ColorPicking::Instance().pick(pos);
	printf("addr selected 0x%x\n",n);
	printf("%f %f \n",pos[0],pos[1]);
}


void App::OnMove(vec2 pos)
{
	AgmdApplication::OnMove(pos);
}

void App::OnKey(a_char key, bool up)
{
	AgmdApplication::OnKey(key,up);
}








